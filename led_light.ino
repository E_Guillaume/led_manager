//version de led manager mais plus opti

#include <LiquidCrystal_I2C.h>
#include <FastLED.h>

#define nbrLeds 300 // 60*5m
#define pinLed1 2

//modes
#define solid 0
#define strombo 1
#define wave 2
#define breath 3
#define wave2 4
#define nbrMod 5


//init potentimetres
#define pinPot1 16
#define pinPot2 15
#define pinPotIntensite 14
int valPotIntensite = 0;
int valPot1 = 0;
int valPot2 = 0;

//init boutons
#define pinBouton1 8
bool valBouton1 = 0;
bool valBouton1Old = 0;

//init variables
unsigned long clck = 10;
bool stromboEtat = 1;
int mode = 0;
int Hwave = 0;//pour les vagues
int Hwave2 = 0;




//init ecran LCD
LiquidCrystal_I2C lcd(0x27,20,4);

//leds
CRGB guirlandeLed1[nbrLeds];

void solidF(){
  for (int i =0; i<nbrLeds; i++)
    guirlandeLed1[i] = CHSV(valPot1/4,valPot2/4,valPotIntensite/4);
  FastLED.show();

}

void stromboF(){



  if ((int)valPot2/100 < 2){
    if ( (int)clck%(int)(valPot2/100) == 0){
      if (stromboEtat){stromboEtat = 0;}else{stromboEtat = 1;}
    }
  }
  else{
    if ( (int)clck%2 == 0){
      if (stromboEtat){stromboEtat = 0;}else{stromboEtat = 1;}
    }
  }


  if (stromboEtat){
    for (int i =0; i<nbrLeds; i++)
      guirlandeLed1[i] = CHSV(valPot1/4,255,valPotIntensite/4);
  }
  else {
    for (int i =0; i<nbrLeds; i++)
      guirlandeLed1[i] = CHSV(0,0,0);
  }
  
  FastLED.show();

  //delay (1023-valPot2);
  
}

void waveF()
{
  int valH = Hwave;
  for (int i =0; i<nbrLeds; i++)
  {
    guirlandeLed1[299-i] = CHSV(valH,255,valPotIntensite/4);
    valH = (valH+1+(int)valPot1/32)%256;
  }
    
  Hwave = (Hwave+1+(int)valPot2/32)%256;

  FastLED.show();
    
}

void wave2F()
{
  int longeur = 10;
  for (int i =0; i<nbrLeds; i++)
  {
    if ((i>Hwave2) && (i<(Hwave2+longeur)))
      guirlandeLed1[i] = CHSV(valPot2/4,255,valPotIntensite/4);
    else
      guirlandeLed1[i] = CHSV(valPot2/4,255,0);
  }

  Hwave2 = (Hwave2+1+(int)valPot1/32)%nbrLeds;

  FastLED.show();
  
}

void breathF()
{
  if (valPot1!=0)
  {
    if (int(clck)%valPot1 == 0)//on incrmente la modification des leds
    {
      Hwave=Hwave+ (int)valPot2/8+1;
      if (Hwave>255){Hwave == 0;}
    }
  }
  else{
    Hwave=Hwave+ (int)valPot2/8+1;
    if (Hwave>255){Hwave == 0;}
  }
    
  
  for (int i =0; i<nbrLeds; i++)
      guirlandeLed1[i] = CHSV(Hwave,255,valPotIntensite/4);

  FastLED.show();
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  lcd.init();
  lcd.backlight();

  pinMode(pinBouton1, INPUT);

  //init LEDS
  FastLED.addLeds<NEOPIXEL,pinLed1>(guirlandeLed1,nbrLeds);

}

void loop() {
  // put your main code here, to run repeatedly:
  valPotIntensite = analogRead(pinPotIntensite);
  valPot1 = analogRead(pinPot1);
  valPot2 = analogRead(pinPot2);
  valBouton1Old = valBouton1;
  valBouton1 = digitalRead(pinBouton1);
  
  //affichage  inputs
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(valPotIntensite);
  lcd.setCursor(5,0);
  lcd.print(valPot1);
  lcd.setCursor(0,1);
  lcd.print(valPot2);
  lcd.setCursor(5,1);
  lcd.print(mode);

  if ((valBouton1Old == 0) &&(valBouton1 == 1)){
    mode++;
  }
    
  mode = mode%nbrMod;

  /*switch (mode){
    case solid :
      solidF();
    case strombo :
      stromboF();
  }*/

  if (mode == solid)
  {
    solidF();
  }
  else if (mode == strombo)
  {
    stromboF();
  }
  else if (mode == wave)
  {
    waveF();
  }
  else if (mode == breath)
  {
    breathF();
  }
  else if (mode == wave2)
  {
    wave2F();  
  }


  clck++;
  delay(1);
  

}
